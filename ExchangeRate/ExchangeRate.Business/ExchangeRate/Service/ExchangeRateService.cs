﻿using AutoMapper;
using ExchangeRate.Business.ExchangeRate.Dto;
using ExchangeRate.Business.ExchangeRate.Enum;
using ExchangeRate.Business.ExchangeRate.Interface;
using ExchangeRate.Data.Entity;
using ExchangeRate.Data.Repositories.Interface;
using Framework.Core.Utility;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ExchangeRate.Business.ExchangeRate.Service
{
    public class ExchangeRateService : IExchangeRateService
    {
        private readonly IExchangeRateRepository _exchangeRateRepository;
        private readonly IMapper _mapper;

        public ExchangeRateService(IExchangeRateRepository exchangeRateRepository, IMapper mapper)
        {
            _exchangeRateRepository = exchangeRateRepository;
            _mapper = mapper;
        }
        public async Task<List<CurrentExchangeRatesResponse>> GetCurrentExchangeRatesAsync(CurrentExchangeRatesRequest request)
        {
            var sortBy = request.SortBy.ToEnum<SortBy>();
            var orderBy = request.OrderBy.ToEnum<OrderBy>();

            var dailyExchangeRates = await _exchangeRateRepository.Get(
                x => x.Date.Date == DateTime.Now.Date);

            var response = _mapper.Map<List<DailyExchangeRate>, List<CurrentExchangeRatesResponse>>(dailyExchangeRates);



            if (SortBy.Code == sortBy)
            {
                if (OrderBy.Ascending == orderBy)
                {
                    response = response.OrderBy(x => x.Currency).ToList();
                }
                else if (OrderBy.Descending == orderBy)
                {
                    response = response.OrderByDescending(x => x.Currency).ToList();
                }
            }
            else if (SortBy.Rate == sortBy)
            {
                if (OrderBy.Ascending == orderBy)
                {
                    response = response.OrderBy(x => x.CurrentRate).ToList();
                }
                else if (OrderBy.Descending == orderBy)
                {
                    response = response.OrderByDescending(x => x.CurrentRate).ToList();
                }
            }

            return response;
        }

        public async Task<List<ExchangeRatesByCurrencyCodeResponse>> GetExchangeRatesByCurrencyCodeAsync(ExchangeRatesByCurrencyCodeRequest request)
        {
            var dailyExchangeRatesByCurCode = await _exchangeRateRepository.Get(x => x.TargetCurrencyCode == request.CurrencyCode);

            dailyExchangeRatesByCurCode = dailyExchangeRatesByCurCode.OrderBy(x => x.Date).ToList();

            var response = _mapper.Map<List<DailyExchangeRate>, List<ExchangeRatesByCurrencyCodeResponse>>(dailyExchangeRatesByCurCode);

            for (int i = 0; i < response.Count; i++)
            {
                if (i.Equals(0))
                {
                    response[i].Changes = "-";
                    continue;
                }

                response[i].Changes = ValuesToPercentage(response[i - 1].Rate, response[i].Rate);
            }

            return response;
        }

        public async Task AddExchangeRateAsync(List<DailyExchangeRate> exchangeRates)
        {
            await _exchangeRateRepository.AddRangeAsync(exchangeRates);
            await _exchangeRateRepository.UnitOfWork.SaveEntitiesAsync();
        }
        private string ValuesToPercentage(decimal oldValue, decimal newValue)
        {
            if (oldValue == 0)
            {
                return null;
            }

            decimal percentage = (newValue - oldValue) / oldValue * 100;

            return $"{percentage.ToString("+0.00;-0.00")}%";

        }
    }
}
