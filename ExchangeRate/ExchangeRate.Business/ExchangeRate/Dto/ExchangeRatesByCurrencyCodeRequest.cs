﻿using System;

namespace ExchangeRate.Business.ExchangeRate.Dto
{
    public class ExchangeRatesByCurrencyCodeRequest
    {
        public string CurrencyCode { get; set; }
    }
}
