﻿using System;

namespace ExchangeRate.Business.ExchangeRate.Dto
{
    public class CurrentExchangeRatesResponse
    {
        public string Currency { get; set; }
        public string LastUpdated { get; set; }
        public decimal CurrentRate { get; set; }

    }
}
