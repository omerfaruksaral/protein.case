﻿using System;

namespace ExchangeRate.Business.ExchangeRate.Dto
{
    public class ExchangeRatesByCurrencyCodeResponse
    {
        public string Currency { get; set; }
        public string Date { get; set; }
        public decimal Rate { get; set; }
        public string Changes { get; set; }
    }
}
