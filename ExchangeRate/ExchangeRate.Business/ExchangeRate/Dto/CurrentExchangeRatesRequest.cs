﻿namespace ExchangeRate.Business.ExchangeRate.Dto
{
    public class CurrentExchangeRatesRequest
    {
        public string SortBy { get; set; }
        public string OrderBy { get; set; }
    }
}
