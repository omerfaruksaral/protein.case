﻿using ExchangeRate.Business.ExchangeRate.Dto;
using ExchangeRate.Business.ExchangeRate.Enum;
using FluentValidation;
using System.Linq;

namespace ExchangeRate.Business.ExchangeRate.Validation
{
    public class ExchangeRatesByCurrencyCodeRequestValidation : AbstractValidator<ExchangeRatesByCurrencyCodeRequest>
    {
        public ExchangeRatesByCurrencyCodeRequestValidation()
        {
            RuleFor(x => x.CurrencyCode).IsEnumName(typeof(CurrencyCode)).NotNull();
        }
    }
}
