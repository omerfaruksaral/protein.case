﻿using ExchangeRate.Business.ExchangeRate.Dto;
using ExchangeRate.Business.ExchangeRate.Enum;
using FluentValidation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ExchangeRate.Business.ExchangeRate.Validation
{
    public class CurrentExchangeRatesRequestValidation : AbstractValidator<CurrentExchangeRatesRequest>
    {
        public CurrentExchangeRatesRequestValidation()
        {
            RuleFor(x => x.SortBy).IsEnumName(typeof(SortBy)).NotNull();
            RuleFor(x => x.OrderBy).IsEnumName(typeof(OrderBy)).NotNull();
        }
    }
}
