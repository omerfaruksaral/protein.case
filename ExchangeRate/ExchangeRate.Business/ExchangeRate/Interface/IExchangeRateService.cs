﻿using ExchangeRate.Business.ExchangeRate.Dto;
using ExchangeRate.Data.Entity;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace ExchangeRate.Business.ExchangeRate.Interface
{
    public interface IExchangeRateService
    {
        Task<List<CurrentExchangeRatesResponse>> GetCurrentExchangeRatesAsync(CurrentExchangeRatesRequest request);
        Task<List<ExchangeRatesByCurrencyCodeResponse>> GetExchangeRatesByCurrencyCodeAsync(ExchangeRatesByCurrencyCodeRequest request);
        Task AddExchangeRateAsync(List<DailyExchangeRate> exchangeRates);
    }
}
