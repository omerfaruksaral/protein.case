﻿namespace ExchangeRate.Business.ExchangeRate.Enum
{
    public enum OrderBy
    {
        Ascending,
        Descending
    }
}
