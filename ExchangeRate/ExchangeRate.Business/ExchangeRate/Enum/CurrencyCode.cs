﻿namespace ExchangeRate.Business.ExchangeRate.Enum
{
    public enum CurrencyCode
    {
        USD,
        EUR,
        GBP,
        CHF,
        KWD,
        SAR,
        RUB,
        TRY
    }

}
