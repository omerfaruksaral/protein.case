﻿namespace ExchangeRate.Business.ExchangeRate.Enum
{
    public enum SortBy
    {
        Code,
        Rate
    }
}
