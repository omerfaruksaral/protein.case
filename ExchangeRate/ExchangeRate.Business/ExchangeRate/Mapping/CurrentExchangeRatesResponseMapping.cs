﻿using AutoMapper;
using ExchangeRate.Business.ExchangeRate.Dto;
using ExchangeRate.Data.Entity;

namespace ExchangeRate.Business.ExchangeRate.Mapping
{
    public class CurrentExchangeRatesResponseMapping : Profile
    {
        public CurrentExchangeRatesResponseMapping()
        {
            CreateMap<DailyExchangeRate, CurrentExchangeRatesResponse>()
               .ForMember(x => x.Currency, x => x.MapFrom(y => $"{y.MainCurrencyCode}-{y.TargetCurrencyCode}"))
               .ForMember(x => x.CurrentRate, y => y.MapFrom(z => z.ForexBuying))
               .ForMember(x => x.LastUpdated, y => y.MapFrom(z => z.Date.ToString("dd'.'MM'.'yyyy")))
               .ReverseMap();
        }
    }
}
