﻿using AutoMapper;
using ExchangeRate.Business.ExchangeRate.Dto;
using ExchangeRate.Data.Entity;

namespace ExchangeRate.Business.ExchangeRate.Mapping
{
    internal class ExchangeRatesByCurrencyCodeResponseMapping : Profile
    {
        public ExchangeRatesByCurrencyCodeResponseMapping()
        {
            CreateMap<DailyExchangeRate, ExchangeRatesByCurrencyCodeResponse>()
              .ForMember(x => x.Currency, x => x.MapFrom(y => $"{y.TargetCurrencyCode}-{y.MainCurrencyCode}"))
              .ForMember(x => x.Rate, y => y.MapFrom(z => z.ForexBuying))
              .ForMember(x => x.Date, y => y.MapFrom(z => z.Date.ToString("dd'.'MM'.'yyyy")))
              .ReverseMap();
        }
    }
}
