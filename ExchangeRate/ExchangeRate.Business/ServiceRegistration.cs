﻿using ExchangeRate.Business.ExchangeRate.Interface;
using ExchangeRate.Business.ExchangeRate.Service;
using FluentValidation.AspNetCore;
using Microsoft.Extensions.DependencyInjection;
using System.Reflection;

namespace ExchangeRate.Business
{
    public static class ServiceRegistration
    {
        public static IServiceCollection AddServicesRegistration(this IServiceCollection services)
        {
            var assemblies = Assembly.GetExecutingAssembly();
            services.AddAutoMapper(assemblies);
            services.AddFluentValidation(x=>
            {
                x.RunDefaultMvcValidationAfterFluentValidationExecutes = false;
                x.RegisterValidatorsFromAssembly(assemblies);
            });
            services.AddScoped<IExchangeRateService, ExchangeRateService>();

            return services;
        }
    }
}
