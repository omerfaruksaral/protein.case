﻿using ExchangeRate.Batch.ExchangeRate;
using Framework.Quartz;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using System;

namespace ExchangeRate.Batch
{
    public static class BatchRegistration
    {
        public static IServiceCollection AddBatch(this IServiceCollection services, IConfiguration configuration)
        {
            services.AddHttpContextAccessor();
            services.AddHttpClient("TCMB", c =>
            {
                c.BaseAddress = new Uri(configuration["TcmbExchangeRateUrl"]);
                c.DefaultRequestHeaders.Add("Accept", "application/xml");
            });
            services.AddSingleton<AddDailyExchangeRatesJob>();

            services.AddSingleton(new JobDefinition
            (
                type: typeof(AddDailyExchangeRatesJob),
                cronExpression: "0 0 9-18 ? * MON-FRI *"
            ));

            return services;
        }
    }
}
