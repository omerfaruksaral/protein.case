﻿using ExchangeRate.Batch.ExchangeRate.Dto;
using ExchangeRate.Business.ExchangeRate.Enum;
using ExchangeRate.Business.ExchangeRate.Interface;
using ExchangeRate.Data.Entity;
using Framework.Core.Utility;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.DependencyInjection;
using Newtonsoft.Json;
using Quartz;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Net.Http;
using System.Threading.Tasks;
using System.Xml;

namespace ExchangeRate.Batch.ExchangeRate
{
    [DisallowConcurrentExecution]
    public class AddDailyExchangeRatesJob : IJob
    {
        private readonly IServiceProvider _serviceProvider;
        private readonly IHttpClientFactory _clientFactory;

        public AddDailyExchangeRatesJob(IServiceProvider serviceProvider, IHttpClientFactory clientFactory)
        {
            _serviceProvider = serviceProvider;
            _clientFactory = clientFactory;
        }
        public async Task Execute(IJobExecutionContext context)
        {
            Common.Logs($"Job is working. [{DateTime.Now.ToString("dd-MMM-yyyy hh:mm:ss")}]");

            List<DailyExchangeRate> dailyExchangeRates = await GetDailyExchangeRates();

            using (var scope = _serviceProvider.CreateScope())
            {
                var service = scope.ServiceProvider.GetService<IExchangeRateService>();
                await service.AddExchangeRateAsync(dailyExchangeRates);
            }
        }

        public async Task<List<DailyExchangeRate>> GetDailyExchangeRates()
        {
            List<DailyExchangeRate> dailyExchangeRates = new List<DailyExchangeRate>();

            using (var client = _clientFactory.CreateClient("TCMB"))
            {
                var request = new HttpRequestMessage(HttpMethod.Get, $"today.xml");// $"202202 / 07022022.xml"
                var response = await client.SendAsync(request);

                if (response.IsSuccessStatusCode)
                {
                    using var responseStream = await response.Content.ReadAsStreamAsync();

                    StreamReader reader = new StreamReader(responseStream);
                    var result = reader.ReadToEnd();

                    var xmlDocument = new XmlDocument();
                    xmlDocument.LoadXml(result);

                    XmlNodeList currencies = xmlDocument.GetElementsByTagName("Currency");

                    XmlNodeList xmlDate = xmlDocument.GetElementsByTagName("Tarih_Date");
                    DateTime exchangeRateDate = DateTime.ParseExact(xmlDate.Item(0).Attributes["Date"].Value, "MM/dd/yyyy", CultureInfo.InvariantCulture);


                    foreach (XmlNode currency in currencies)
                    {
                        var currencyCode = currency.Attributes["Kod"].Value.ToUpper();
                        if (currencyCode.IsDefined<CurrencyCode>())
                        {
                            var serializedXmlNode = JsonConvert.SerializeXmlNode(
                            currency,
                            Newtonsoft.Json.Formatting.Indented,
                            true);

                            var exchangeRateDto = JsonConvert.DeserializeObject<DailyExchangeRateDto>(serializedXmlNode);

                            dailyExchangeRates.Add(new DailyExchangeRate
                            {
                                MainCurrencyCode = nameof(CurrencyCode.TRY),
                                TargetCurrencyCode = currency.Attributes["Kod"].Value,
                                BanknoteBuying = exchangeRateDto.BanknoteBuying,
                                BanknoteSelling = exchangeRateDto.BanknoteSelling,
                                CrossRate = exchangeRateDto.CrossRateUSD,
                                Date = exchangeRateDate,
                                ForexBuying = exchangeRateDto.ForexBuying,
                                ForexSelling = exchangeRateDto.ForexSelling,
                                Unit = exchangeRateDto.Unit,
                            });
                        }
                    }
                }
            }

            return dailyExchangeRates;
        }
    }
}
