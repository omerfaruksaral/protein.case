﻿using ExchangeRate.Data.Context;
using ExchangeRate.Data.Repositories;
using ExchangeRate.Data.Repositories.Interface;
using Framework.Core.Repository;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;

namespace ExchangeRate.Data
{
    public static class ServiceRegistration
    {
        public static IServiceCollection AddDataRegistration(this IServiceCollection services, IConfiguration configuration)
        {
            services.AddDbContext<ExchangeRateDbContext>(opt =>
            {
                opt.UseSqlServer(configuration["DbConnectionString"]);
                opt.EnableSensitiveDataLogging();
            });

            services.AddScoped<IUnitOfWork>(x => x.GetRequiredService<ExchangeRateDbContext>());

            services.AddScoped<IExchangeRateRepository, ExchangeRateRepository>();

            var optionsBuilder = new DbContextOptionsBuilder<ExchangeRateDbContext>()
                .UseSqlServer(configuration["DbConnectionString"]);

            using var dbContext = new ExchangeRateDbContext(optionsBuilder.Options);
            dbContext.Database.EnsureCreated();
            dbContext.Database.Migrate();

            return services;
        }
    }
}
