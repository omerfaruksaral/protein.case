﻿using ExchangeRate.Data.Context;
using ExchangeRate.Data.Entity;
using ExchangeRate.Data.Repositories.Interface;
using Framework.Core.Repository;

namespace ExchangeRate.Data.Repositories
{
    public class ExchangeRateRepository : GenericRepository<DailyExchangeRate, ExchangeRateDbContext>, IExchangeRateRepository
    {
        private readonly ExchangeRateDbContext dbContext;
        private readonly IUnitOfWork unitOfWork;

        public ExchangeRateRepository(ExchangeRateDbContext dbContext, IUnitOfWork unitOfWork) : base(dbContext, unitOfWork)
        {
            this.dbContext = dbContext;
            this.unitOfWork = unitOfWork;
        }


    }
}
