﻿using ExchangeRate.Data.Context;
using ExchangeRate.Data.Entity;
using Framework.Core.Repository;

namespace ExchangeRate.Data.Repositories.Interface
{
    public interface IExchangeRateRepository : IGenericRepository<DailyExchangeRate, ExchangeRateDbContext>
    {
    }
}
