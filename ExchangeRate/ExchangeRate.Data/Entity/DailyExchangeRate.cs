﻿using Framework.Core.Entity;
using System;

namespace ExchangeRate.Data.Entity
{
    public class DailyExchangeRate : BaseEntity
    {
        public string MainCurrencyCode { get; set; }
        public string TargetCurrencyCode { get; set; }
        public short Unit { get; set; }
        public decimal? ForexBuying { get; set; }
        public decimal? ForexSelling { get; set; }
        public decimal? BanknoteBuying { get; set; }
        public decimal? BanknoteSelling { get; set; }
        public decimal? CrossRate { get; set; }

        public DateTime Date { get; set; }
    }
}
