﻿using ExchangeRate.Data.Context;
using ExchangeRate.Data.Entity;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace ExchangeRate.Data.EntityConfigurations
{
    public class DailyExchangeRateEntityConfiguration : IEntityTypeConfiguration<DailyExchangeRate>
    {
        public void Configure(EntityTypeBuilder<DailyExchangeRate> builder)
        {
            builder.ToTable("daily_exchange_rates", ExchangeRateDbContext.DEFAULT_SCHEMA);

            //builder.HasKey(i => i.Id);
            //builder.Property(i => i.Id).ValueGeneratedOnAdd();

            builder.Property(i => i.Id).HasColumnName("guid");
            builder.Property(i => i.MainCurrencyCode).HasColumnName("main_currency_code").HasMaxLength(3);
            builder.Property(i => i.TargetCurrencyCode).HasColumnName("target_currency_code").HasMaxLength(3);
            builder.Property(i => i.Unit).HasColumnName("unit");
            builder.Property(i => i.ForexBuying).HasColumnName("forex_buying");
            builder.Property(i => i.ForexSelling).HasColumnName("forex_selling");
            builder.Property(i => i.BanknoteBuying).HasColumnName("banknote_buying");
            builder.Property(i => i.BanknoteSelling).HasColumnName("banknote_selling");
            builder.Property(i => i.CrossRate).HasColumnName("cross_rate");
            builder.Property(i => i.Date).HasColumnName("date").IsRequired();
        }
    }
}
