﻿using ExchangeRate.Data.Entity;
using ExchangeRate.Data.EntityConfigurations;
using Framework.Core.Repository;
using Microsoft.EntityFrameworkCore;
using System.Threading;
using System.Threading.Tasks;

namespace ExchangeRate.Data.Context
{
    public class ExchangeRateDbContext : DbContext, IUnitOfWork
    {
        public const string DEFAULT_SCHEMA = "exr";

        public ExchangeRateDbContext(DbContextOptions<ExchangeRateDbContext> options) : base(options)
        {

        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.ApplyConfiguration(new DailyExchangeRateEntityConfiguration());
        }

        public async Task<bool> SaveEntitiesAsync(CancellationToken cancellationToken = default)
        {
            return await base.SaveChangesAsync(cancellationToken) > 0;
        }

        public DbSet<DailyExchangeRate> DailyExchangeRate { get; set; }
    }
}
