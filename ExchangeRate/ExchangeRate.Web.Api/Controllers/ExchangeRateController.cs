﻿using ExchangeRate.Business.ExchangeRate.Dto;
using ExchangeRate.Business.ExchangeRate.Interface;
using ExchangeRate.Data.Entity;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace ExchangeRate.Web.Api.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ExchangeRateController : ControllerBase
    {
        private readonly IExchangeRateService _exchangeRateService;

        public ExchangeRateController(IExchangeRateService exchangeRateService)
        {
            _exchangeRateService = exchangeRateService;
        }

        [HttpGet("GetExchangeRatesByDate")]
        public async Task<List<ExchangeRatesByCurrencyCodeResponse>> GetExchangeRatesByDate([FromQuery] ExchangeRatesByCurrencyCodeRequest request)
        {
            return await _exchangeRateService.GetExchangeRatesByCurrencyCodeAsync(request);
        }

        [HttpGet("GetCurrentExchangeRates")]
        public async Task<List<CurrentExchangeRatesResponse>> GetCurrentExchangeRates([FromQuery] CurrentExchangeRatesRequest request)
        {
            return await _exchangeRateService.GetCurrentExchangeRatesAsync(request);
        }

        [HttpPost("AddExchangeRate")]
        public async Task AddExchangeRate([FromBody] List<DailyExchangeRate> request)
        {
            await _exchangeRateService.AddExchangeRateAsync(request);
        }

    }
}
