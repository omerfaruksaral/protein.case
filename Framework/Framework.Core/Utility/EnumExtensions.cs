﻿using System;

namespace Framework.Core.Utility
{
    public static class EnumExtensions
    {
        public static T ToEnum<T>(this string stringName) where T : struct
        {
            if (string.IsNullOrEmpty(stringName))
            {
                new ArgumentException("stringName", $"Enum:{typeof(T).FullName} stringName is null or empty");
            }

            stringName = stringName.Trim();

            if (stringName.Length == 0)
            {
                new ArgumentException($"Must specify valid information for parsing Enum:{typeof(T).FullName} in the string");
            }

            return Enum.Parse<T>(stringName);
        }
        public static bool IsDefined<T>(this string name)
        {
            return Enum.IsDefined(typeof(T), name);
        }
    }
}
