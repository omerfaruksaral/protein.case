﻿using System.IO;

namespace Framework.Core.Utility
{
    public class Common
    {
        public static void Logs(string message)
        {
            var path = Path.Combine(Directory.GetCurrentDirectory(), "tmp", "quartzlogs");
            var filePath = Path.Combine(path, "logs.txt");
            FileStream fileStream;

            if (!Directory.Exists(path))
            {
                Directory.CreateDirectory(path);
                fileStream = new FileStream(filePath, FileMode.Create);
            }
            else
            {
                fileStream = new FileStream(filePath, FileMode.Append);
            }

            using TextWriter textWriter = new StreamWriter(fileStream);

            textWriter.WriteLine(message);
        }
    }
}
