﻿namespace Framework.Core.Repository
{
    public interface IRepository<T>
    {
        IUnitOfWork UnitOfWork { get; }
    }
}
