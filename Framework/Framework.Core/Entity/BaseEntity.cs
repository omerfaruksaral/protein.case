﻿using System;

namespace Framework.Core.Entity
{
    public class BaseEntity
    {
        public virtual Guid Id { get; protected set; }
    }
}
