﻿using Framework.Core.Utility;
using System;

namespace Framework.Quartz
{
    public class JobDefinition
    {
        public JobDefinition(Type type, string cronExpression)
        {
            Common.Logs($"Job is defined. [{DateTime.Now.ToString("dd-MMM-yyyy hh:mm:ss")}]");
            Type = type;
            CronExpression = cronExpression;
        }

        public Type Type { get; }
        public string CronExpression { get; set; }

    }

}


