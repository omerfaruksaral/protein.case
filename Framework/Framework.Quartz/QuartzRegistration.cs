﻿using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Quartz;
using Quartz.Impl;
using Quartz.Spi;

namespace Framework.Quartz
{
    public static class QuartzRegistration
    {
        public static IServiceCollection AddQuartz(this IServiceCollection services, IConfiguration configuration)
        {
            services.AddHostedService<QuartzHostedService>();
            services.AddSingleton<IJobFactory, SingletonJobFactory>();
            services.AddSingleton<ISchedulerFactory, StdSchedulerFactory>();

            return services;
        }
    }
}