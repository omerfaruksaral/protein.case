﻿using Framework.Core.Utility;
using Microsoft.Extensions.Hosting;
using Quartz;
using Quartz.Spi;
using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;

namespace Framework.Quartz
{
    public class QuartzHostedService : IHostedService
    {
        private readonly ISchedulerFactory _schedulerFactory;
        private readonly IJobFactory _jobFactory;
        private readonly IEnumerable<JobDefinition> _jobs;

        public QuartzHostedService(ISchedulerFactory schedulerFactory,
            IJobFactory jobFactory,
            IEnumerable<JobDefinition> jobs)
        {
            _schedulerFactory = schedulerFactory;
            _jobFactory = jobFactory;
            _jobs = jobs;
        }

        public IScheduler Scheduler { get; set; }

        public async Task StartAsync(CancellationToken cancellationToken)
        {
            Common.Logs($"Host is started. [{DateTime.Now.ToString("dd-MMM-yyyy hh:mm:ss")}]");

            Scheduler = await _schedulerFactory.GetScheduler(cancellationToken);
            Scheduler.JobFactory = _jobFactory;
            foreach (var _job in _jobs)
            {
                var job = CreateJob(_job);
                var trigger = CreateTrigger(_job);
                await Scheduler.ScheduleJob(job, trigger, cancellationToken);
            }

            await Scheduler.Start(cancellationToken);
        }

        public async Task StopAsync(CancellationToken cancellationToken)
        {
            Common.Logs($"Host is shutdown. [{DateTime.Now.ToString("dd-MMM-yyyy hh:mm:ss")}]");

            await Scheduler?.Shutdown(cancellationToken);
        }

        private static IJobDetail CreateJob(JobDefinition myJob)
        {
            var type = myJob.Type;
            return JobBuilder.Create(type)
                .WithIdentity(type.FullName)
                .WithDescription(type.Name)
                .Build();
        }

        private static ITrigger CreateTrigger(JobDefinition myJob)
        {
            return TriggerBuilder.Create()
                .WithIdentity($"{myJob.Type.FullName}.trigger")
                .WithCronSchedule(myJob.CronExpression)
                .WithDescription($"{myJob.Type.Name}.trigger")
                .Build();
        }
    }
}
