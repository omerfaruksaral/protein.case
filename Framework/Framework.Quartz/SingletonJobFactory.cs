﻿using Framework.Core.Utility;
using Microsoft.Extensions.DependencyInjection;
using Quartz;
using Quartz.Spi;
using System;

namespace Framework.Quartz
{
    public class SingletonJobFactory : IJobFactory
    {
        private readonly IServiceProvider _serviceProvider;
        public SingletonJobFactory(IServiceProvider serviceProvider)
        {
            _serviceProvider = serviceProvider;
        }
        public IJob NewJob(TriggerFiredBundle bundle, IScheduler scheduler)
        {
            Common.Logs($"{bundle.JobDetail.JobType.Name} is started. [{DateTime.Now.ToString("dd-MMM-yyyy hh:mm:ss")}]");

            return _serviceProvider.GetRequiredService(bundle.JobDetail.JobType) as IJob;
        }

        public void ReturnJob(IJob job)
        {
            Common.Logs($"{job.GetType().Name} is completed. [{DateTime.Now.ToString("dd-MMM-yyyy hh:mm:ss")}]");
        }
    }
}
